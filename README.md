**Prediction of Wine quality**

This model is trained using the dataset [dataset link](https://www.kaggle.com/uciml/red-wine-quality-cortez-et-al-2009)

Two types of scaling is checked in preprocessing:
1. MinMaxScaler()
2. StandardScaler()

We have tested out model for 2 algorithems:
1. K-NN Model
2. Support Vector Machine (SVM)